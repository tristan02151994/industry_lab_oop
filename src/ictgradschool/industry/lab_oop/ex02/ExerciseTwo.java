package ictgradschool.industry.lab_oop.ex02;

import ictgradschool.Keyboard;

import java.util.Random;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        String lnum = Keyboard.readInput();
        String unum = Keyboard.readInput();
        int lnumIn = Integer.parseInt(lnum);
        int unumIn = Integer.parseInt(unum);
        int n1= (int)(Math.random()*(unumIn-lnumIn)+ lnumIn);
        int n2= (int)(Math.random()*(unumIn-lnumIn)+ lnumIn);
        int n3= (int)(Math.random()*(unumIn-lnumIn)+ lnumIn);
        int minNum=Math.min(Math.min(n1,n2),n3);
        System.out.println("Lower bound ? "+lnumIn);
        System.out.println("Upper bound ? "+unumIn);
        System.out.println("3 randomly generated numbers:"+n1+", "+n2+" and "+n3);
        System.out.println("Smallest number is "+minNum);




    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
